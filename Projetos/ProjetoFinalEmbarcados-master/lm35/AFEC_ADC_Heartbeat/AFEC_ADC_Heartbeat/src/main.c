#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "asf.h"
#include "fifo.h"
#include "heartbeat_4md69.h"

	
/** Reference voltage for AFEC,in mv. */
#define VOLT_REF        (3300)

/** The maximal digital value */
#define MAX_DIGITAL     (4095UL)

#define STRING_EOL    "\r"
#define STRING_HEADER "-- AFEC Temperature Sensor Example --\r\n" \
		"-- "BOARD_NAME" --\r\n" \
		"-- Compiled: "__DATE__" "__TIME__" --"STRING_EOL

volatile long g_systimer = 0;

void SysTick_Handler() {
	g_systimer++;	
}

/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{

	/* Configura USART1 Pinos */
	sysclk_enable_peripheral_clock(ID_PIOB);
	sysclk_enable_peripheral_clock(ID_PIOA);
	pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4);  // RX
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;
	
	const usart_serial_options_t uart_serial_options = {
		.baudrate   = CONF_UART_BAUDRATE,
		.charlength = CONF_UART_CHAR_LENGTH,
		.paritytype = CONF_UART_PARITY,
		.stopbits   = CONF_UART_STOP_BITS,
	};

	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}



/**
 * \brief Application entry point.
 *
 * \return Unused (ANSI-C compatibility).
 */
int main(void)
{
	/* Initialize the SAM system. */
	sysclk_init();
	board_init();
	configure_console();
    
	pio_configure(PIOC, PIO_OUTPUT_1, 1 << 8, PIO_DEFAULT);
	SysTick_Config(sysclk_get_cpu_hz() / 1000); // 1 ms
	
	/* Output example information. */
	puts(STRING_HEADER);
	
	h4d69_init();
	h4d69_enable_interrupt();
	
	int batimentos = 0;
	int counter = 0;
	
	long time_start = g_systimer;
	while (1) {
		h4d69_update();
		if(h4d69_has_beat()) {
			pio_clear(PIOC, 1 << 8);
			printf("beat\n\r");
			batimentos++;
		} else {
			pio_set(PIOC, 1 << 8);
		}
		
		if(abs(g_systimer - time_start) > 10000) { // 30seg
			printf("%d bpm \n\r", batimentos * 6);
			batimentos = 0;
			time_start = g_systimer;
		}
		h4d69_convert();
	
	}
}
