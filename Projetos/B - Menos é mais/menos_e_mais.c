#include "asf.h"

/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/

/**
 *  Informacoes para o RTC
 *  poderia ser extraida do __DATE__ e __TIME__
 *  ou ser atualizado pelo PC.
 */

/**
* LEDs
*/
#define LED_PIO_ID	   ID_PIOB
#define LED_PIO        PIOB
#define LED_PIN		   1
#define LED_PIN_MASK   (1<<LED_PIN)

#define USART_COM     USART1
#define USART_COM_ID  ID_USART1

#define TC0_CHANNEL 1
#define TC0_MODE TC_CMR_WAVE
#define TC0_INTERRUPT_SOURCE (TC_IER_CPCS | TC_IER_CPAS)


/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile uint8_t flag_led0 = 1;
volatile uint32_t g_led_blink = 1;
volatile uint32_t g_usart_transmission_done = 0;

// GLOBALS
uint32_t g_count = 0;
uint8_t g_bufferRX[3];
uint8_t g_bufferTX[3];


/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void LED_init(int estado);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void RTC_init(void);
void pin_toggle(Pio *pio, uint32_t mask);

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

/**
*  Interrupt handler for TC1 interrupt.
*/
void TC1_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrupção foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 1);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if(flag_led0){ //Toda vez que a interrupção TC acontecer, o led piscará 1 vez. Isso dependerá da frequência utilizada na inicialização do Init na main.
		pin_toggle(LED_PIO, LED_PIN_MASK);
    }
}

/**
* \brief Interrupt handler for the RTC. Refresh the display.
*/
void RTC_Handler(void) //O RTC não está sendo utilizado.
{
	uint32_t ul_status = rtc_get_status(RTC);

	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
	}
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
			rtc_clear_status(RTC, RTC_SCCR_ALRCLR);

			flag_led0 = 0;
			pmc_disable_periph_clk(ID_TC1);
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	
}

void USART1_Handler(void) {
  uint32_t ret = usart_get_status(USART_COM);
  uint8_t c = NULL;

  if(ret & US_IER_RXRDY) {
    usart_serial_getchar(USART_COM, &c);
    if(c != '\n') {
      g_bufferRX[g_count++] = c;
    }
    else {
      g_bufferRX[g_count] = 0x00;
      g_usart_transmission_done = 1;
      g_count = 0;
    }
  }
}

static void configure_rtt(void)
{
  uint32_t ul_previous_time;

  /* Configure RTT for a 1 second tick interrupt */
  #if SAM4N || SAM4S || SAM4E || SAM4C || SAM4CP || SAM4CM || SAMV71 || SAMV70 || SAME70 || SAMS70
  rtt_sel_source(RTT, false);
  #endif
  rtt_init(RTT, 32768);

  ul_previous_time = rtt_read_timer_value(RTT);
  while (ul_previous_time == rtt_read_timer_value(RTT));

  /* Enable RTT interrupt */
  NVIC_DisableIRQ(RTT_IRQn);
  NVIC_ClearPendingIRQ(RTT_IRQn);
  NVIC_SetPriority(RTT_IRQn, 0);
  NVIC_EnableIRQ(RTT_IRQn);
  rtt_enable_interrupt(RTT, RTT_MR_RTTINCIEN);
}


/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

/**
*  Toggle pin controlado pelo PIO (out)
*/
void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask)){
	pio_clear(pio, mask);
  } else{
	pio_set(pio,mask);}
}
/**
* @Brief Inicializa o pino do LED
*/
void LED_init(int estado){
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_set_output(LED_PIO, LED_PIN_MASK, estado, 0, 0 );
}


uint32_t usart_puts(uint8_t *pstring) {
  uint32_t i = 0 ;

  while(*(pstring + i)){
    usart_serial_putchar(USART_COM, *(pstring+i++));
    while(!uart_is_tx_empty(USART_COM)){};
  }
  return(i);
}

static void USART1_init(void) {

  /* Configura USART1 Pinos */
  sysclk_enable_peripheral_clock(ID_PIOB);
  sysclk_enable_peripheral_clock(ID_PIOA);
  pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4);  // RX
  pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
  MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;

  /* Configura opcoes USART */
  const sam_usart_opt_t usart_settings = {
    .baudrate     = 115200,
    .char_length  = US_MR_CHRL_8_BIT,
    .parity_type  = US_MR_PAR_NO,
    .stop_bits    = US_MR_NBSTOP_1_BIT,
    .channel_mode = US_MR_CHMODE_NORMAL
  };

  /* Ativa Clock periferico USART0 */
  sysclk_enable_peripheral_clock(USART_COM_ID);

  /* Configura USART para operar em modo RS232 */
  usart_init_rs232(USART_COM, &usart_settings, sysclk_get_peripheral_hz());

  /* Enable the receiver and transmitter. */
  usart_enable_tx(USART_COM);
  usart_enable_rx(USART_COM);

  usart_enable_interrupt(USART_COM, US_IER_RXRDY);
  NVIC_EnableIRQ(USART_COM_ID);

}
/**
* Configura TimerCounter (TC) para gerar uma interrupcao no canal (ID_TC e TC_CHANNEL)
* na taxa de especificada em freq.
*/
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	/* O TimerCounter é meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrupçcão no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_WAVE); 
	tc_write_ra(TC, TC_CHANNEL, ((ul_sysclk / ul_div) / (freq*10))); //Aqui é definido os dois valores de ra e rc que a onda de alimentação assumirá. Eles tem exatamente 1 de diferença entre si. Esse foi o valor escolhido para contribuir com a otimização enegética por ser o menor valor int possível.
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / (freq*10-1));

	/* Configura e ativa interrupçcão no TC canal 0 */
	/* Interrupção no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, (TC_IER_CPCS | TC_IER_CPAS));

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}

/**
* Configura o RTC para funcionar com interrupcao de alarme
*/
void RTC_init(){

	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);
	
	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN);

}

/************************************************************************/
/* Main Code	                                                        */
/************************************************************************/
int main(void){
	/* Initialize the SAM system */
	sysclk_init();

	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;

	/* Configura Leds */
	LED_init(0);

	/** Configura RTC */
	RTC_init();

	LED_init(1);
  	TC1_init(2);
 	USART1_init();

	/** Configura timer TC0, canal 1 */
	TC_init(TC0, ID_TC1, 1, 0.667); //Essa frequência de 0.667 foi a escolhida para que o led pisque 4 vezes em 3 segundos, sendo esse cálculo f = 4/(3*2)
	
	pin_toggle(LED_PIO, LED_PIN_MASK);

	while (1) {

		if(g_usart_transmission_done) {
      		if(g_bufferRX[0] == 't') {
        		g_led_blink = !g_led_blink;

       		 	if(g_led_blink);
         			 usart_puts("LIGOU\n");
       			else
          			usart_puts("APAGOU\n");
      }

      g_usart_transmission_done = 0;
    }
		/* Entrar em modo sleep */
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI); // O modo sleep permite um menor gasto energético enquanto nenhuma interrupção acontece.
	}

}

