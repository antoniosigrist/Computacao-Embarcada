/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>


#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

#define STRING_EOL    "\r"
#define STRING_HEADER "-- AFEC Temperature Sensor Example --\r\n" \
"-- "BOARD_NAME" --\r\n" \
"-- Compiled: "__DATE__" "__TIME__" --"STRING_EOL

/** Reference voltage for AFEC,in mv. */
#define VOLT_REF        (3300)

/** The maximal digital value */
/** 2^12 - 1                  */
#define MAX_DIGITAL     (4095UL)

volatile bool is_conversion_done = false;

volatile uint32_t g_ul_value = 0;

/* Canal do sensor de temperatura */
#define AFEC_CHANNEL_TEMP_SENSOR 11
/**
* LED
*/

#define LED2_PIO PIOC
#define LED2_PIO_ID ID_PIOC
#define LED2_PIN 30
#define LED2_PIN_MASK (1 << LED2_PIN)

/**
* Botes
*/
#define BUT1_PIO PIOD
#define BUT1_PIO_ID ID_PIOD
#define BUT1_PIN 30
#define BUT1_PIN_MASK (1 << BUT1_PIN)
#define BUT1_DEBOUNCING_VALUE 79

#define BUT2_PIO PIOC
#define BUT2_PIO_ID ID_PIOC
#define BUT2_PIN 31
#define BUT2_PIN_MASK (1 << BUT2_PIN)
#define BUT2_DEBOUNCING_VALUE 79

/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/


volatile bool flag_conversion = false;
volatile bool flab_button1 = true;

volatile bool flab_alarm = false;
volatile bool temp2_flag = false;
volatile bool temp3_flag = false;



/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void BUT_init(void);
void LED_init(int estado);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void pin_toggle(Pio *pio, uint32_t mask);

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

/**
*  Handler Interrupcao botao 1
*/
int temp2 = 0;

static void Button1_Handler(uint32_t id, uint32_t mask){
/*	
	flab_button1 = true;
	temp2 = temp2 + 5;
	
	while(flab_button1 == true){
		
	char new_temperature[10];
		
		sprintf(new_temperature, "set:%d", temp2);
	
		//gfx_mono_draw_string(new_temperature, 0, 0, &sysfont);
		flab_button1 = false;
	}*/
	temp2_flag == true;
}

static void Button2_Handler(uint32_t id, uint32_t mask){
	
	/*temp2 = temp2 - 5;
		
	char new_temperature[10];
		
	sprintf(new_temperature, "set:%d", temp2);

	*/
	temp3_flag == true;
}


/**
* \brief Interrupt handler for the RTC. Refresh the display.
*/

/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

/**
*  Toggle pin controlado pelo PIO (out)
*/
void pin_toggle(Pio *pio, uint32_t mask){
	pio_set(pio,mask);
}

/**
* @Brief Inicializa o pino do BUT
*/
void BUT_init(void){
	pmc_enable_periph_clk(BUT1_PIO_ID);
	pio_set_input(BUT1_PIO, BUT1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
		
	pio_enable_interrupt(BUT1_PIO, BUT1_PIN_MASK);
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);
		
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 1);
/*
	pmc_enable_periph_clk(BUT2_PIO_ID);
	pio_set_input(BUT2_PIO, BUT2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
		
	pio_enable_interrupt(BUT2_PIO, BUT2_PIN_MASK);
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, PIO_IT_FALL_EDGE, Button2_Handler);
		
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 1);*/
};

/**
* @Brief Inicializa o pino do LED
*/
 


void LED_init(int estado){
	
	pmc_enable_periph_clk(LED2_PIO_ID);
	pio_set_output(LED2_PIO, LED2_PIN_MASK, estado, 0, 0 );
};

/**
* Configura o RTC para funcionar com interrupcao de alarme
*/

static int32_t convert_adc_to_temp(int32_t ADC_value){

  int32_t ul_vol;
  int32_t ul_temp;

	ul_vol = ADC_value * VOLT_REF / MAX_DIGITAL;

  /*
   * According to datasheet, The output voltage VT = 0.72V at 27C
   * and the temperature slope dVT/dT = 2.33 mV/C
   */
  ul_temp = (ul_vol - 720)  * 100 / 233 + 27;
  
  return(ul_temp);
}

static void AFEC_Temp_callback(void)
{
	g_ul_value = afec_channel_get_value(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
	is_conversion_done = true;
}

int main (void)
{
	board_init();
	sysclk_init();
	delay_init();
	gfx_mono_ssd1306_init();

	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;

	/* Configura Leds */
	LED_init(1);

	/* Configura os botes */
	BUT_init();
  
	pio_set(LED2_PIO, 0);
	
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);

	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);

	/* configura call back */
 	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_11,	AFEC_Temp_callback, 1);

	/*** Configuracao especfica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, &afec_ch_cfg);

	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	* down to 0.
	*/
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, 0x200);

	/***  Configura sensor de temperatura ***/
	struct afec_temp_sensor_config afec_temp_sensor_cfg;

	afec_temp_sensor_get_config_defaults(&afec_temp_sensor_cfg);
	afec_temp_sensor_set_config(AFEC0, &afec_temp_sensor_cfg);

	/* Selecina canal e inicializa converso */
	afec_channel_enable(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
	afec_start_software_conversion(AFEC0);
	
	uint32_t ul_temp;
	
	int temp;
	int temp2
	
	while(1) {
		
		char temperature[10];
		
		ul_temp = convert_adc_to_temp(g_ul_value);
		
		temp = (int) ul_temp;
		
		sprintf(temperature, "%doC__", temp);
		
		if(temp2_flag == true)
		{
			temp2+=5;
			char new_temperature[10];
		
		sprintf(new_temperature, "set:%d", temp2);
	
		gfx_mono_draw_string(new_temperature, 0, 0, &sysfont);
		temp2_flag == false;
		}
		
		if(temp3_flag == true)
		{
			temp2-=5;
			char new_temperature[10];
		
		sprintf(new_temperature, "set:%d", temp2);
	
		gfx_mono_draw_string(new_temperature, 0, 0, &sysfont);
		temp3_flag == false;
		}
		
		if(is_conversion_done == true) {
			is_conversion_done = false;
			
			afec_start_software_conversion(AFEC0);
			delay_s(1);

			if (temp>temp2){
			pin_toggle(LED2_PIO, LED2_PIN_MASK); 
			}
		}
		
		
	}
		
	
}
