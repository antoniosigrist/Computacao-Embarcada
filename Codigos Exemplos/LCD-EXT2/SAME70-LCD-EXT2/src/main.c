#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"
#include <assert.h>

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

struct ili9488_opt_t g_ili9488_display_opt;

/** Reference voltage for AFEC,in mv. */
#define VOLT_REF        (3300)

/** The maximal digital value */
/** 2^12 - 1                  */
#define MAX_DIGITAL     (4095UL)

#define STRING_EOL    "\n"
#define STRING_HEADER "-- Temperature Sensor --\n\n" \
"-- "BOARD_NAME" --\n\n" \
"-- Compiled: "__DATE__" "__TIME__" --"STRING_EOL

#define SENSOR_AFEC         AFEC0
#define SENSOR_AFEC_ID      ID_AFEC0
#define SENSOR_AFEC_CH      AFEC_CHANNEL_5 // Pin PB2
#define SENSOR_AFEC_CH_IR   AFEC_INTERRUPT_EOC_5
#define SENSOR_THRESHOLD    50


volatile bool is_conversion_done = false;

volatile uint32_t g_ul_value = 0;

/* Canal do sensor de temperatura */
#define AFEC_CHANNEL_TEMP_SENSOR 11
/**
* LED
*/

#define LED2_PIO PIOC
#define LED2_PIO_ID ID_PIOC
#define LED2_PIN 30
#define LED2_PIN_MASK (1 << LED2_PIN)

/**
* Bot�es
*/
#define BUT1_PIO PIOD
#define BUT1_PIO_ID ID_PIOD
#define BUT1_PIN 28
#define BUT1_PIN_MASK (1 << BUT1_PIN)
#define BUT1_DEBOUNCING_VALUE 79

/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile bool flag_conversion = false;

/**
}
/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	

	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

static void config_ADC_TEMP(void){
/************************************* 
   * Ativa e configura AFEC
   *************************************/  
  /* Ativa AFEC - 0 */
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);
  
	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);
  
	/* configura call back */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_5,	AFEC_Temp_callback, 1); 
   
	/*** Configuracao especfica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, SENSOR_AFEC_CH, &afec_ch_cfg);
  
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	afec_channel_set_analog_offset(AFEC0,SENSOR_AFEC_CH, 0x200);
	
	/* Selecina canal e inicializa converso */  
	afec_channel_enable(AFEC0, SENSOR_AFEC_CH);
		NVIC_SetPriority(AFEC0_IRQn, 10);
	afec_enable_interrupt(AFEC0, SENSOR_AFEC_CH);
	NVIC_EnableIRQ(AFEC0_IRQn);

}




static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */


int ul_temp2 = 0;

static int32_t convert_adc_to_temp(int32_t ADC_value){

  int32_t ul_vol;
  int32_t ul_temp;

	ul_vol = ADC_value * VOLT_REF / MAX_DIGITAL;

  /*
   * According to datasheet, The output voltage VT = 0.72V at 27C
   * and the temperature slope dVT/dT = 2.33 mV/C
   */
  ul_temp = (ul_vol - 720)  * 100 / 233 + 27;
  
  return(ul_temp);
}

void SENSOR_init(void);

void SENSOR_init(void){
	pmc_enable_periph_clk(SENSOR_AFEC_ID);
};





static void AFEC_Temp_callback(void)
{
	g_ul_value = afec_channel_get_value(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
	is_conversion_done = true;
}




int main(void)
{
	// array para escrita no LCD
	uint8_t stingLCD[256];
	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	delay_init();
	
	SENSOR_init();
	config_ADC_TEMP();


/* Output example information. */
puts(STRING_HEADER);
afec_start_software_conversion(AFEC0);

/* buffer para recebimento de dados */
uint8_t bufferRX[100];
uint8_t bufferTX[100];

	USART0_init();
		/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	delay_init( sysclk_get_cpu_hz());
	
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);

	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);

	/* configura call back */
 	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_11,	AFEC_Temp_callback, 1);

	/*** Configuracao espec�fica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, &afec_ch_cfg);

	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	* down to 0.
	*/
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, 0x200);

	/***  Configura sensor de temperatura ***/
	struct afec_temp_sensor_config afec_temp_sensor_cfg;

	afec_temp_sensor_get_config_defaults(&afec_temp_sensor_cfg);
	afec_temp_sensor_set_config(AFEC0, &afec_temp_sensor_cfg);

	/* Selecina canal e inicializa convers�o */
	afec_channel_enable(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
	afec_start_software_conversion(AFEC0);
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);

    /* Inicializa e configura o LCD */
	configure_lcd();

    /* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));


	uint32_t ul_temp;

	int temp;
	int ul_temp2 = 0;
	uint32_t ul_vol;

while(1) {
	
	char temperature[10];
	
	ul_temp = convert_adc_to_temp(g_ul_value);
	
	temp = (int) ul_temp;
	
	sprintf(temperature, "%d oC", temp);
	ili9488_draw_string(10, 300, temperature);

	delay_s(5);
	
	temperature[0] = ' \n ';
	
	if(is_conversion_done == true) {
		is_conversion_done = false;

			ul_temp = ((float)ul_vol/10.0);
		
		afec_start_software_conversion(AFEC0);

		delay_s(1);
	}
	



}
	return 0;
}
