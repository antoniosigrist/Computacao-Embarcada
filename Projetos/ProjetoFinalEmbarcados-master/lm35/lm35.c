#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "asf.h"

/** Reference voltage for AFEC,in mv. */
#define VOLT_REF        (3300)

/** The maximal digital value */
#define MAX_DIGITAL     (4095UL)

#define STRING_EOL    "\r"
#define STRING_HEADER "-- Temperature Sensor --\r\n" \
		"-- "BOARD_NAME" --\r\n" \
		"-- Compiled: "__DATE__" "__TIME__" --"STRING_EOL

#define SENSOR_AFEC         AFEC0
#define SENSOR_AFEC_ID      ID_AFEC0
#define SENSOR_AFEC_CH      AFEC_CHANNEL_5 // Pin PB2
#define SENSOR_AFEC_CH_IR   AFEC_INTERRUPT_EOC_5
#define SENSOR_THRESHOLD    50

/** The conversion data is done flag */
volatile bool is_conversion_done = false;

/** The conversion data value */
volatile uint32_t g_ul_value = 0;


void SENSOR_init(void);

void SENSOR_init(void){
	pmc_enable_periph_clk(SENSOR_AFEC_ID);
};

/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#ifdef CONF_UART_CHAR_LENGTH
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#ifdef CONF_UART_STOP_BITS
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

/**
 * \brief AFEC interrupt callback function.
 */
static void AFEC_Temp_callback(void)
{
	g_ul_value = afec_channel_get_value(SENSOR_AFEC, SENSOR_AFEC_CH);
	is_conversion_done = true;
};

/**
 * \brief Application entry point.
 *
 * \return Unused (ANSI-C compatibility).
 */

static void config_ADC_TEMP(void){
/************************************* 
   * Ativa e configura AFEC
   *************************************/  
  /* Ativa AFEC - 0 */
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);
  
	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);
  
	/* configura call back */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_5,	AFEC_Temp_callback, 1); 
   
	/*** Configuracao especfica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, SENSOR_AFEC_CH, &afec_ch_cfg);
  
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	afec_channel_set_analog_offset(AFEC0,SENSOR_AFEC_CH, 0x200);
	
	/* Selecina canal e inicializa converso */  
	afec_channel_enable(AFEC0, SENSOR_AFEC_CH);
		NVIC_SetPriority(AFEC0_IRQn, 10);
	afec_enable_interrupt(AFEC0, SENSOR_AFEC_CH);
	NVIC_EnableIRQ(AFEC0_IRQn);

}



int main(void)
{
	uint32_t ul_vol;
	uint32_t ul_temp;

	/* Initialize the SAM system. */
	sysclk_init();
	board_init();
	SENSOR_init();
	config_ADC_TEMP();

	configure_console();

	/* Output example information. */
	puts(STRING_HEADER);
	afec_start_software_conversion(AFEC0);

	while (1) {

		if(is_conversion_done == true) {
			
			ul_vol = g_ul_value * VOLT_REF / MAX_DIGITAL;

			ul_temp = (5000 * ul_vol)/1024;

			printf("Temperature is: %4d\r", (int)ul_temp);
			is_conversion_done = false;
			afec_start_software_conversion(AFEC0);
		}
	}
}
